# Links for 3D Printing

## Sites for models
* Thingiverse: https://www.thingiverse.com

## 3D Modeling software

* TinkerCAD: https://www.tinkercad.com/
  * Completely online
  * I used this for simple things
* Fusion360: https://www.autodesk.com/products/fusion-360/personal
  * Free for three years
  * Offline
  * I haven't used this
* SketchUp: https://www.sketchup.com/
  * I used to use this, but they've changed owners a few times and stopped using it a while ago

## Slicers
This converts the 3D model (e.g. a `.stl` file) into the gcode file that contains all of the commands for the printer.

* Cura: https://ultimaker.com/software/ultimaker-cura

## Other stuff
* Octoprint: https://octoprint.org
  * Best used with a Raspberry Pi to control and monitor the printer
  


